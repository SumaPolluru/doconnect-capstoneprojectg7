package com.app.AdminMicroservice.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import com.app.AdminMicroservice.repository.AdminRepository;
import com.app.AdminMicroservice.service.AdminService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import com.app.AdminMicroservice.model.Admin;
import com.app.AdminMicroservice.model.UserData;

//http:localhost:8081-- Admin MicroService Starts
@RestController
public class AdminController
{

	@Autowired
	AdminService adminService;

	@Autowired
	AdminRepository adminRepository;
	
	@Autowired  
	RestTemplate template;
	
	org.springframework.http.HttpHeaders headers =new org.springframework.http.HttpHeaders();

	HttpEntity<String> entity = new HttpEntity<String>(headers);

	// Swagger
	@ApiOperation(value = "Admin List", notes = "This method will retreive list from database", nickname = "getAdmin")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),
	@ApiResponse(code = 404, message = "Admin not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Admin.class, responseContainer = "List") })
	
	//User JWT Token
	@RequestMapping(value = "/userData")
	public String getUserList()
	{
		headers.setBearerAuth("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdW1hQGdtYWlsLmNvbSIsImV4cCI6MTY2OTY0NjI4NywiaWF0IjoxNjY5NjI4Mjg3fQ.8t9OHoJHRQ1nJarOTiG-brybADRsJTrcMO9oaPY6f6zf6J_bwFp9nrXeUPq41oTWExd9S3Mp1Iqi3V7ZP2CYoA");

		return template.exchange("http://localhost:8082/approvedQuestions", HttpMethod.GET, entity, String.class).getBody();
	}
	
	// Here, Web page starts
	@RequestMapping("/") 
	public ModelAndView Welcome() 
	{
		return new ModelAndView("doConnect");
	}

	// Webpage Home
	@RequestMapping("/home") 
	public ModelAndView Welcome(HttpServletRequest request) 
	{
		request.setAttribute("adminobj", "ADMIN_HOME");
		return new ModelAndView("adminpage");
	}

	//AdminOperations Home
	@RequestMapping("/adminHome")
	public ModelAndView Admin(HttpServletRequest request) 
	{
		request.setAttribute("adminobj", "HOME");
		return new ModelAndView("adminWelcomePage");
	}

	//Registration
	@RequestMapping("/registeration")
	public ModelAndView registration(HttpServletRequest request) 
	{
		request.setAttribute("adminobj", "ADMIN_REGISTER");
		return new ModelAndView("adminpage");
	}

	// Registration,checking that admin is present or not,Then only its save
	@RequestMapping("/save-admin") 
	public ModelAndView registerUser(@ModelAttribute Admin admin, BindingResult bindingResult, HttpServletRequest request) 
	{
		if (adminService.findByAdminName(admin.getAdminName()) != null) 
		{
			request.setAttribute("error", "AdminName already Exist");
			request.setAttribute("adminobj", "ADMIN_REGISTER");
			return new ModelAndView("adminpage");
		} 
		else 
		{
			adminService.saveAdmin(admin);
			request.setAttribute("adminobj", "ADMIN_HOME");
			return new ModelAndView("adminpage");
		}
	}

	//Login
	@RequestMapping("/adminLogin")
	public ModelAndView login(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_LOGIN");
		return new ModelAndView("adminpage");
	}

	// Checking Login Credentials
	@RequestMapping("/login-admin") 
	public ModelAndView loginUser(@ModelAttribute Admin admin, HttpServletRequest request) 
	{
		if (adminService.findByAdminNameAndAdminPassword(admin.getAdminName(), admin.getAdminPassword()) != null)
		{
			request.setAttribute("adminobj", "HOME");
			return new ModelAndView("adminWelcomePage");
		} 
		else 
		{
			request.setAttribute("error", "Invalid Username or Password");
			request.setAttribute("adminobj", "ADMIN_LOGIN");
			return new ModelAndView("adminpage");

		}
	}

	// Here, Registration data should store
	@RequestMapping("/saveadmin") 
	public String saveUser(@RequestParam String adminName, @RequestParam String adminPassword) 
	{
		Admin admin = new Admin(adminName, adminPassword);
		adminService.saveAdmin(admin);
		return "Admin Saved";
	}
	
	//Logout
	@RequestMapping("/logout")
	public ModelAndView logout(HttpServletRequest request) 
	{
			HttpSession httpSession = request.getSession();
			httpSession.invalidate();
			return new ModelAndView("redirect:/");
	}
	
	//CRUD Operations on Admin
	@GetMapping("/show-admins") 
	public ModelAndView showAllAdmins(HttpServletRequest request) 
	{
		request.setAttribute("admins", adminService.showAllAdmins());
		request.setAttribute("mode", "ALL_ADMINS");
		return new ModelAndView("adminWelcomePage");
	}

	@RequestMapping("/delete-admin")
	public ModelAndView deleteUser(@RequestParam int adminId, HttpServletRequest request) 
	{
		adminService.deleteMyAdmin(adminId);
		request.setAttribute("admins", adminService.showAllAdmins());
		request.setAttribute("mode", "ALL_ADMINS");
		return new ModelAndView("adminWelcomePage");
	}

	@RequestMapping("/edit-admin")
	public ModelAndView editUser(@RequestParam int adminId, HttpServletRequest request) 
	{
		request.setAttribute("admin", adminService.editAdmin(adminId));
		request.setAttribute("mode", "MODE_UPDATE");
		return new ModelAndView("adminWelcomePage");
	}

	@RequestMapping("/saveUpdation")
	public ModelAndView saveUpdates(@ModelAttribute("mode") Admin admin) 
	{
		adminRepository.save(admin);
		return new ModelAndView("adminWelcomePage");
	}
	
	//Here,Retrieving the User Details using Rest Template
	@GetMapping("/show-users")
	public String data() 
	{
		return template.exchange("http://localhost:8082/data", HttpMethod.GET, entity, String.class).getBody();
	}
	
	//CRUD Operations on User
	@RequestMapping("/edit-user")
	public String update(@RequestParam int userId)
	{
		return template.exchange("http://localhost:8082/update/" +userId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	@RequestMapping("/saveUserUpdation")
	public String saveUpdate(@ModelAttribute UserData user) 
	{
		return template.exchange("http://localhost:8082/saveUpdation/" +user, HttpMethod.POST, entity, String.class).getBody();	
	}
	
	@RequestMapping("/delete-user")
	public String delete(@RequestParam int userId)
	{
		return template.exchange("http://localhost:8082/delete/" +userId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	//Retrieving Approved Questions from Questions Module
	@RequestMapping("/showApprovedQuestions")
	public String question() 
	{
		return template.exchange("http://localhost:8082/approvedQuestions", HttpMethod.GET, entity, String.class).getBody();
	}

	//Retrieving UnApproved Questions from Questions Module
	@RequestMapping("/showUnApprovedQuestions")
	public String questions()
	{
		return template.exchange("http://localhost:8082/unApprovedQuestions", HttpMethod.GET, entity, String.class).getBody();
	}

	//Approving the Question
	@RequestMapping("/approveQuestions")
	public String approve(@RequestParam int questionId) 
	{
		return template.exchange("http://localhost:8082/approveQuestion/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	//Approved Answers
	@RequestMapping("/showApprovedAnswers")
	public String answer() 
	{
		return template.exchange("http://localhost:8082/approvedAnswerData", HttpMethod.GET, entity, String.class).getBody();
	}

	@RequestMapping("/showUnApprovedAnswers")
	public String answers() 
	{
		return template.exchange("http://localhost:8082/unApprovedAnswers", HttpMethod.GET, entity, String.class).getBody();
	}

	@RequestMapping("/approveAnswers")
	public String approveAnswers(@RequestParam int answerId)
	{
		return template.exchange("http://localhost:8082/approveAnswer/" +answerId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	//Delete Operation on Inappropriate Questions or Answers
	@RequestMapping("/delete-question")
	public String deleteQuestion(@RequestParam int questionId)
	{
		return template.exchange("http://localhost:8082/deleteInappropriateQuestions/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	@RequestMapping("/delete-answer")
	public String deleteAnswer(@RequestParam int answerId) 
	{
		return template.exchange("http://localhost:8082/deleteInappropriateAnswers/" +answerId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	@RequestMapping("/delete-InappropriateAnswer")
	public String deleteInappropriate(@RequestParam int answerId) 
	{
		return template.exchange("http://localhost:8082/deleteInappropriateAnswersApproved/" +answerId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	@RequestMapping("/delete-InappropriateQuestion")
	public String deleteInappropriateQuestion(@RequestParam int questionId) 
	{
		return template.exchange("http://localhost:8082/deleteInappropriateQuestionsApproved/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	//Particular Question Answer
	@RequestMapping("/getAnswerData")
	public String getAnswer(@RequestParam int questionId) 
	{
		return template.exchange("http://localhost:8082/getQuestionAnswer/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
}
