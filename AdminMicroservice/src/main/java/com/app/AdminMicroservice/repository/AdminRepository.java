package com.app.AdminMicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.AdminMicroservice.model.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> 
{
	//Queries
	@Query("Select adobj from Admin adobj where adobj.adminName=?1 and adobj.adminPassword=?2")
	public Admin getAdmin(String adminName,String adminPassword);
	
	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword);
	
	public Admin findByAdminName(String adminName);
	
}

