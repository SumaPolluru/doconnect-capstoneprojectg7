package com.app.AdminMicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.AdminMicroservice.model.Admin;
import com.app.AdminMicroservice.repository.AdminRepository;

@Service
@Transactional
public class AdminService 
{

	@Autowired
	private AdminRepository adminRepository;

	public AdminService(AdminRepository adminRepository) 
	{
		this.adminRepository = adminRepository;
	}

	public Admin saveAdmin(Admin admin) 
	{
		return adminRepository.save(admin);
	}

	//Login Credentials Checking
	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword)
	{
		return adminRepository.findByAdminNameAndAdminPassword(adminName, adminPassword);
	}

	// Register credentials checking
	public Admin findByAdminName(String adminName) 
	{
		return adminRepository.findByAdminName(adminName);
	}

	//CRUD Operations
	public List<Admin> showAllAdmins() 
	{
		List<Admin> admins = new ArrayList<Admin>();
		for (Admin admin : adminRepository.findAll()) {
			admins.add(admin);
		}

		return admins;
	}

	public void deleteMyAdmin(int adminId)
	{
		adminRepository.deleteById(adminId);
	}

	public Admin editAdmin(int adminId)
	{
		Optional<Admin> uobj = adminRepository.findById(adminId);
		return uobj.get();
	}

}
