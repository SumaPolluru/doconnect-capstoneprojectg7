<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color:white;
	text-align: center;
	text-size:5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/adminHome" class="navbar-brand"><font color=white size=4px>Welcome|Admin</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/show-admins"><font color=white size=4px>Admins</font></a></li>
					<li><a href="/show-users"><font color=white size=4px>Users</font></a></li>
					<li><a href="/showApprovedQuestions"><font color=white size=4px>Approved Questions</font></a></li>
					<li><a href="/showUnApprovedQuestions"><font color=white size=4px>UnApproved Questions</font></a></li>
					<li><a href="/showApprovedAnswers"><font color=white size=4px>Approved Answers</font></a></li>
					<li><a href="/showUnApprovedAnswers"><font color=white size=4px>UnApproved Answers</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${adminobj=='HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to Admin Operations Page</font>
					</h2>
					<h3>These Operations are Performed By Admin</h3>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='ALL_ADMINS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>All Admins</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Admin Id</th>
								<th>Admin Name</th>
								<th>Admin Password</th>
								<th>Delete</th>
								<th>Edit</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="admin" items="${admins}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${admin.adminId}</td>
									<td>${admin.adminName}</td>
									<td>${admin.adminPassword}</td>
									<td><a href="/delete-admin?adminId=${admin.adminId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-admin?adminId=${admin.adminId }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		
		<c:when test="${mode=='MODE_UPDATE' }">
			<div class="container text-center">
				<h3>Update User</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="saveUpdation">
				<div class="form-group">
						<label class="control-label col-md-3"></label>
						<div class="col-md-7">
							<input type="hidden" class="form-control" name="adminId"
								value="${admin.adminId }" placeholder="AdminId" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Admin Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="adminName"
								value="${admin.adminName }" placeholder="adminName" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="adminPassword"
								value="${admin.adminPassword }" placeholder="Password" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>