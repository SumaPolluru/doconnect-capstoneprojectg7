package com.app.DeleteMicroservice.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
//http:localhost:8083 -- Delete MicroService Starts
@RestController
public class AnswerController 
{
	@Autowired 
	RestTemplate template;
	org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();

	HttpEntity<String> entity = new HttpEntity<String>(headers);

	//Webpage Starts
	@RequestMapping("/") 
	public ModelAndView welcome(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_HOME");
		return new ModelAndView("deleteOperation");
	}
	
	//Webpage Home
	@RequestMapping("/adminHome") 
	public ModelAndView welcomeHome(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_HOME");
		return new ModelAndView("deleteOperation");
	}

	//Retrieving Approved Answers
	@RequestMapping("/showApprovedAnswers")
	public String answer() 
	{
		return template.exchange("http://localhost:8082/approvedAnswerData", HttpMethod.GET, entity, String.class).getBody();
	}

	//Retrieving UnApproved Answers
	@RequestMapping("/showUnApprovedAnswers")
	public String answers() 
	{
		return template.exchange("http://localhost:8082/unApprovedAnswers", HttpMethod.GET, entity, String.class).getBody();
	}

	//Approve operation
	@RequestMapping("/approveAnswers")
	public String approveAnswers(@RequestParam int answerId) 
	{
		return template.exchange("http://localhost:8082/approveAnswer/" + answerId, HttpMethod.POST, entity, String.class).getBody();
	}

	//Get Particular answer
	@RequestMapping("/getAnswerData")
	public String getAnswer(@RequestParam int questionId) 
	{
		return template.exchange("http://localhost:8082/getQuestionAnswer/" + questionId, HttpMethod.POST, entity,
				String.class).getBody();
	}
	
	//Delete Inappropriate Answers
	@RequestMapping("/delete-answer")
	public String deleteAnswer(@RequestParam int answerId) 
	{
		return template.exchange("http://localhost:8082/deleteInappropriateAnswers/" + answerId, HttpMethod.POST,
				entity, String.class).getBody();
	}

	@RequestMapping("/delete-InappropriateAnswer")
	public String deleteInappropriate(@RequestParam int answerId) 
	{
		return template.exchange("http://localhost:8082/deleteInappropriateAnswersApproved/" + answerId,
				HttpMethod.POST, entity, String.class).getBody();
	}

	//Logout
	@RequestMapping("/logout")
	public String logout() 
	{
		return template.exchange("http://localhost:8082/logout", HttpMethod.POST, entity, String.class).getBody();
	}

}
