package com.app.DeleteMicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class QuestionController 
{

	@Autowired  
	RestTemplate template;
	org.springframework.http.HttpHeaders headers =new org.springframework.http.HttpHeaders();

	HttpEntity<String> entity = new HttpEntity<String>(headers);

	//Retrieving Approved Questions
	@RequestMapping("/showApprovedQuestions")
	public String question() 
	{
		return template.exchange("http://localhost:8082/approvedQuestions", HttpMethod.GET, entity, String.class).getBody();
	}
	
	//Retrieving UnApproved Questions
	@RequestMapping("/showUnApprovedQuestions")
	public String questions() 
	{
		return template.exchange("http://localhost:8082/unApprovedQuestions", HttpMethod.GET, entity, String.class).getBody();
	}

	//Approve Operation
	@RequestMapping("/approveQuestions")
	public String approve(@RequestParam int questionId)
	{
		return template.exchange("http://localhost:8082/approveQuestion/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	//Delete Inappropriate Questions
	@RequestMapping("/delete-question")
	public String deleteQuestion(@RequestParam int questionId) 
	{
		return template.exchange("http://localhost:8082/deleteInappropriateQuestions/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
	
	@RequestMapping("/delete-InappropriateQuestion")
	public String deleteInappropriateQuestion(@RequestParam int questionId)
	{
		return template.exchange("http://localhost:8082/deleteInappropriateQuestionsApproved/" +questionId, HttpMethod.POST, entity, String.class).getBody();
	}
}
