package com.app.DeleteMicroservice.model;

public class Question 
{
	private int questionId;
	private String questionTopic;
	private String question;
	private int answerId;
	private int userId;

	public int getAnswerId() 
	{
		return answerId;
	}

	public void setAnswerId(int answerId) 
	{
		this.answerId = answerId;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId) 
	{
		this.userId = userId;
	}

	public int getQuestionId() 
	{
		return questionId;
	}

	public void setQuestionId(int questionId) 
	{
		this.questionId = questionId;
	}

	public String getQuestionTopic() 
	{
		return questionTopic;
	}

	public void setQuestionTopic(String questionTopic) 
	{
		this.questionTopic = questionTopic;
	}

	public String getQuestion() 
	{
		return question;
	}

	public void setQuestion(String question) 
	{
		this.question = question;
	}
}
