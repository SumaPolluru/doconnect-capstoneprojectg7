<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color: white;
	text-align: center;
	text-size: 5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/" class="navbar-brand"><font color=white
				size=4px>Welcome|Admin</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/showApprovedQuestions"><font color=white
							size=4px>Approved Questions</font></a></li>
					<li><a href="/showUnApprovedQuestions"><font color=white
							size=4px>UnApproved Questions</font></a></li>
					<li><a href="/showApprovedAnswers"><font color=white
							size=4px>Approved Answers</font></a></li>
					<li><a href="/showUnApprovedAnswers"><font color=white
							size=4px>UnApproved Answers</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>

		<c:when test="${adminobj=='ADMIN_HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to DoConnect </font>
					</h2>
					<h3>Delete Operation on Inappropriate Questions and Answers</h3>
				</div>
			</div>

		</c:when>
	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>