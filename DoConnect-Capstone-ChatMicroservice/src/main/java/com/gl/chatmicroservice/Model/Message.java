package com.gl.chatmicroservice.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
public class Message 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long messageId;
	private String message;
	private String fromUser;
	private String toUsername; // Email
	private String toUser; //User Name
	
	public String getToUser() 
	{
		return toUser;
	}

	public void setToUser(String toUser)
	{
		this.toUser = toUser;
	}

	public Long getMessageId()
	{
		return messageId;
	}

	public void setMessageId(Long messageId)
	{
		this.messageId = messageId;
	}

	public String getToUsername() 
	{
		return toUsername;
	}

	public void setToUsername(String toUsername)
	{
		this.toUsername = toUsername;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getFromUser()
	{
		return fromUser;
	}

	public void setFromUser(String fromUser) 
	{
		this.fromUser = fromUser;
	}

	public Message()
	{

	}

	public Message(Long messageId, String message, String fromUser, String toUsername) {
		super();
		this.messageId = messageId;
		this.message = message;
		this.fromUser = fromUser;
		this.toUsername = toUsername;
	}
	
}
