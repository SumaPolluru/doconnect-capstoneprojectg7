package com.gl.chatmicroservice.controller;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.chatmicroservice.controller.dto.MessageDto;
import com.gl.chatmicroservice.service.MessageService;

//http://localhost:8084/swagger-ui.html

@RestController
@RequestMapping("/chat")
@CrossOrigin
public class ChatController {

	@Autowired
	private MessageService messageService;

	@PostMapping("/sendMessage")
	public MessageDto sendMessage(@Valid @RequestBody MessageDto messageDTO) {
		return messageService.sendMessage(messageDTO);
	}

	@GetMapping("/getMessage")
	public List<MessageDto> getMessage() {
		return messageService.getMessage();
	}

}
