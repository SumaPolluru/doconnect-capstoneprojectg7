package com.gl.chatmicroservice.controller.dto;

public class ResponseDto 
{
	public ResponseDto()
	{
		
	}

	private String response;

	public String getResponse() 
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

	public ResponseDto(String response) 
	{
		super();
		this.response = response;
	}
}
