package com.gl.chatmicroservice.service;

import java.util.List;

import javax.validation.Valid;

import com.gl.chatmicroservice.controller.dto.MessageDto;

public interface MessageService 
{
	public MessageDto sendMessage(@Valid MessageDto messageDTO);

	public List<MessageDto> getMessage();
}
