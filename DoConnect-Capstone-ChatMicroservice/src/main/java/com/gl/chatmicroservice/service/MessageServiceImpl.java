package com.gl.chatmicroservice.service;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gl.chatmicroservice.Model.Message;
import com.gl.chatmicroservice.controller.dto.MessageDto;
import com.gl.chatmicroservice.repository.MessageRepository;

//Business Logic
@Service
public class MessageServiceImpl implements MessageService 
{
	@Autowired
	private MessageRepository messageRepo;

	@Override
	public MessageDto sendMessage(@Valid MessageDto messageDTO) 
	{

		Message message = new Message();
		message.setMessage(messageDTO.getMessage());
		message.setFromUser(messageDTO.getFromUser());
		message.setToUsername(messageDTO.getToUserName());
		message.setToUser(messageDTO.getToUser());
		message = messageRepo.save(message);
		messageDTO.setFromUser(message.getFromUser());
		messageDTO.setMessage(message.getMessage());
		messageDTO.setToUserName(message.getToUsername());
		messageDTO.setToUser(message.getToUser());
		return messageDTO;
	}

	@Override
	public List<MessageDto> getMessage()
	{

		List<Message> messages = messageRepo.findAll();
		List<MessageDto> data = new ArrayList<MessageDto>();
		for (Message message : messages) {
			MessageDto messageDTO = new MessageDto();
			messageDTO.setFromUser(message.getFromUser());
			messageDTO.setMessage(message.getMessage());
			messageDTO.setToUserName(message.getToUsername());
			messageDTO.setToUser(message.getToUser());
			data.add(messageDTO);

		}

		return data;
	}

}
