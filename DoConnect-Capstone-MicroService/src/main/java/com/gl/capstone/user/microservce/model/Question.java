package com.gl.capstone.user.microservce.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "Question")
public class Question {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotBlank(message = "Question can not be blank")
	private String question;
	@OneToOne
	private User user;
	@NotBlank(message = "provide the topic")
	private String topic;
	private Boolean isApproved = false;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public Boolean getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}
	public Question(Long id, @NotBlank(message = "Question can not be blank") String question, User user,
			@NotBlank(message = "provide the topic") String topic, Boolean isApproved) {
		super();
		this.id = id;
		this.question = question;
		this.user = user;
		this.topic = topic;
		this.isApproved = isApproved;
	}
	
	public Question()
	{
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private int questionId;
//	private String questionTopic;
//	private String question;
//	private Boolean isApproved = false;
//	
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "userid", referencedColumnName = "userId")
//	private User user;
//	
//	
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "anwId", referencedColumnName = "answerId")
//	private Answer answer;
//	
//
//	
//	
//	public User getUser() {
//		return user;
//	}
//	public void setUser(User user) {
//		this.user = user;
//	}
//	public Answer getAnswer() {
//		return answer;
//	}
//	public void setAnswer(Answer answer) {
//		this.answer = answer;
//	}
//	
//	
//	
//	public int getQuestionId() {
//		return questionId;
//	}
//	public void setQuestionId(int questionId) {
//		this.questionId = questionId;
//	}
//	public String getQuestionTopic() {
//		return questionTopic;
//	}
//	public void setQuestionTopic(String questionTopic) {
//		this.questionTopic = questionTopic;
//	}
//	public String getQuestion() {
//		return question;
//	}
//	public void setQuestion(String question) {
//		this.question = question;
//	}
//	public Boolean getIsApproved() {
//		return isApproved;
//	}
//	public void setIsApproved(Boolean isApproved) {
//		this.isApproved = isApproved;
//	}

}
