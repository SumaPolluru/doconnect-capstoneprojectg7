package com.gl.capstone.user.microservce.service;

import java.util.List;

import com.gl.capstone.user.microservce.dto.*;
import com.gl.capstone.user.microservce.model.*;

public interface AdminService {
	public List<Question> getUnApprovedQuestions();

	public List<Answer> getUnApprovedAnswers();

	public Question approveQuestion(Long questionId);

	public Answer approveAnswer(Long answerId);

	public ResponseDTO deleteQuestion(Long questionId);

	public ResponseDTO deleteAnswer(Long answerId);

	public User getUser(String email);

	public List<User> getAllUser();
}
