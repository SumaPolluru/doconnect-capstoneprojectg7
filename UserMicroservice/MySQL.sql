
mysql> create database DoConnectDB;
Query OK, 1 row affected (0.04 sec)

mysql> use DoConnectDB;
Database changed
mysql> Show tables;
Empty set (0.03 sec)

mysql> use doconnectdb;
Database changed
mysql> show tables;
+-----------------------+
| Tables_in_doconnectdb |
+-----------------------+
| admin                 |
| answer                |
| question              |
| user_data             |
+-----------------------+
4 rows in set (0.00 sec)

mysql> desc admin;
+----------------+--------------+------+-----+---------+----------------+
| Field          | Type         | Null | Key | Default | Extra          |
+----------------+--------------+------+-----+---------+----------------+
| admin_id       | int          | NO   | PRI | NULL    | auto_increment |
| admin_name     | varchar(255) | YES  |     | NULL    |                |
| admin_password | varchar(255) | YES  |     | NULL    |                |
+----------------+--------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)

mysql> desc user_data;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| user_id  | int          | NO   | PRI | NULL    | auto_increment |
| password | varchar(255) | YES  |     | NULL    |                |
| username | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)

mysql> desc answer;
+----------------------+--------------+------+-----+---------+----------------+
| Field                | Type         | Null | Key | Default | Extra          |
+----------------------+--------------+------+-----+---------+----------------+
| answer_id            | int          | NO   | PRI | NULL    | auto_increment |
| answer               | varchar(255) | YES  |     | NULL    |                |
| is_approved          | bit(1)       | YES  |     | NULL    |                |
| answer_user_user_id  | int          | YES  | MUL | NULL    |                |
| question_question_id | int          | YES  | MUL | NULL    |                |
+----------------------+--------------+------+-----+---------+----------------+
5 rows in set (0.01 sec)

mysql> desc question;
+----------------+--------------+------+-----+---------+----------------+
| Field          | Type         | Null | Key | Default | Extra          |
+----------------+--------------+------+-----+---------+----------------+
| question_id    | int          | NO   | PRI | NULL    | auto_increment |
| is_approved    | bit(1)       | YES  |     | NULL    |                |
| question       | varchar(255) | YES  |     | NULL    |                |
| question_topic | varchar(255) | YES  |     | NULL    |                |
| user_user_id   | int          | YES  | MUL | NULL    |                |
+----------------+--------------+------+-----+---------+----------------+
5 rows in set (0.01 sec)
