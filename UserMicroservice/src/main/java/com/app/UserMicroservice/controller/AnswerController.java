package com.app.UserMicroservice.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.app.UserMicroservice.dto.PostAnswer;
import com.app.UserMicroservice.model.Answer;
import com.app.UserMicroservice.model.UserData;
import com.app.UserMicroservice.repository.AnswerRepository;
import com.app.UserMicroservice.repository.QuestionRepository;
import com.app.UserMicroservice.repository.UserRepository;
import com.app.UserMicroservice.service.AnswerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

//http:localhost:8082-- User MicroService Starts
/*After Generating Token, Use that Token in Admin
 * Use Below URL for User operations
 * http:localhost:8082/adminHome
 * */
@RestController
public class AnswerController 
{

	@Autowired
	UserRepository userRepository;

	@Autowired
	AnswerService answerService;

	@Autowired
	QuestionRepository questionRepository;

	@Autowired
	AnswerRepository answerRepository;

	// Swagger
	@ApiOperation(value = "Answer List", notes = "This method will retrieve list from database", nickname = "getAnswer")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),@ApiResponse(code = 404, message = "Answer not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = UserData.class, responseContainer = "List") })

	//Answer Form for Taking answer from user
	@RequestMapping("/answerForm")
	public ModelAndView answer(HttpServletRequest request) 
	{
		request.setAttribute("userobj", "USER_ANSWER");
		return new ModelAndView("userWelcomePage");
	}

	//Post Answer Operation
	@RequestMapping("/postAnswer")
	public ModelAndView answer(@ModelAttribute PostAnswer postAnswer, BindingResult bindingResult,
			HttpServletRequest request) 
	{
		if (userRepository.findByUserId(postAnswer.getUserId()) != null
				&& questionRepository.findByQuestionId(postAnswer.getQuestionId()) != null) 
		{
			request.setAttribute("answer", answerService.giveAnswer(postAnswer));
			request.setAttribute("mode", "POST_ANSWER");
			return new ModelAndView("userWelcomePage");
		} else 
		{
			request.setAttribute("error", "QuestionID Or UserID Invalid,Please Try Again");
			request.setAttribute("userobj", "USER_ANSWER");
			return new ModelAndView("userWelcomePage");
		}
	}

	//Particular Question Answer
	@RequestMapping("/getAnswers")
	public ModelAndView getAnswers(@RequestParam int questionId, HttpServletRequest request) 
	{
		List<Answer> alist = answerRepository.getAnswer(questionId);
		request.setAttribute("mode", "GET_ANSWER");
		return new ModelAndView("userWelcomePage", "answerdata", alist);
	}

	//Approve Operation for Admin
	@RequestMapping("/approveAnswer/{answerId}")
	public ModelAndView approveAnswer(@PathVariable("answerId") int answerId, HttpServletRequest request) 
	{
		request.setAttribute("answer", answerService.approveAnswer(answerId));
		request.setAttribute("mode", "APPROVE_ANSWER");
		return new ModelAndView("adminOperations");
	}

	//Retrieving Approved Answers with Delete Operation for Admin
	@RequestMapping("/approvedAnswerData")
	public ModelAndView approvedAnswers(HttpServletRequest request) 
	{
		List<Answer> ulist = answerService.getApprovedAnswers();
		request.setAttribute("mode", "ANSWER_ADMIN");
		return new ModelAndView("adminOperations", "answerdata", ulist);
	}
	
	//Retrieving Approved Answers for User
	@RequestMapping("/approvedAnswers")
	public ModelAndView answers(HttpServletRequest request) 
	{
		List<Answer> ulist = answerService.getApprovedAnswers();
		request.setAttribute("mode", "ANSWER_USER");
		return new ModelAndView("userWelcomePage", "answerdata", ulist);
	}

	//UnApproved Answers
	@RequestMapping("/unApprovedAnswers")
	public ModelAndView showAlAnswers(HttpServletRequest request) 
	{
		List<Answer> ulist = answerService.getUnApprovedAnswers();
		request.setAttribute("mode", "UNAPPROVED_ANSWERS");
		return new ModelAndView("adminOperations", "answerdata", ulist);
	}

	//Delete Inappropriate Answers Code Implementation
	@RequestMapping("/deleteInappropriateAnswers/{answerId}")
	public ModelAndView deleteAnswers(@PathVariable("answerId") int answerId, HttpServletRequest request) 
	{
		answerService.deleteAnswer(answerId);
		List<Answer> ulist = answerService.getUnApprovedAnswers();
		request.setAttribute("mode", "UNAPPROVED_ANSWERS");
		return new ModelAndView("adminOperations", "answerdata", ulist);
	}	
	
	@RequestMapping("/deleteInappropriateAnswersApproved/{answerId}")
	public ModelAndView delete(@PathVariable("answerId") int answerId, HttpServletRequest request)
	{
		answerService.deleteAnswer(answerId);
		List<Answer> ulist = answerService.getApprovedAnswers();
		request.setAttribute("mode", "ANSWER_DATA");
		return new ModelAndView("adminOperations", "answerdata", ulist);
	}	
	
	@RequestMapping("/getQuestionAnswer/{questionId}")
	public ModelAndView getAnswer(@PathVariable("questionId") int questionId, HttpServletRequest request)
	{
		List<Answer> alist = answerRepository.getAnswer(questionId);
		request.setAttribute("mode", "QUESTION_ANSWER");
		return new ModelAndView("adminOperations", "answerdata", alist);
	}

}
