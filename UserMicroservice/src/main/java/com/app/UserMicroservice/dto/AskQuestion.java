package com.app.UserMicroservice.dto;

public class AskQuestion 
{
	private int userId;
	private String question;
	private String questionTopic;
	private Boolean isApproved = false;

	public Boolean getIsApproved() 
	{
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved) 
	{
		this.isApproved = isApproved;
	}

	public int getUserId() 
	{
		return userId;
	}

	public void setUserId(int userId) 
	{
		this.userId = userId;
	}

	public String getQuestion() 
	{
		return question;
	}

	public void setQuestion(String question) 
	{
		this.question = question;
	}

	public String getQuestionTopic() 
	{
		return questionTopic;
	}

	public void setQuestionTopic(String questionTopic) 
	{
		this.questionTopic = questionTopic;
	}

}
