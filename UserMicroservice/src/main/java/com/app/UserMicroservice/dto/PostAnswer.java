package com.app.UserMicroservice.dto;

public class PostAnswer 
{
	private int userId;
	private int questionId;
	private String answer;

	public int getUserId() 
	{
		return userId;
	}

	public void setUserId(int userId) 
	{
		this.userId = userId;
	}

	public int getQuestionId() 
	{
		return questionId;
	}

	public void setQuestionId(int questionId) 
	{
		this.questionId = questionId;
	}

	public String getAnswer() 
	{
		return answer;
	}

	public void setAnswer(String answer)
	{
		this.answer = answer;
	}

}
