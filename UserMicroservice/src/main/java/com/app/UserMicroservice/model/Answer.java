package com.app.UserMicroservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Answer")
public class Answer 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int answerId;
	private String answer;
	private Boolean isApproved = false;
	@ManyToOne
	private UserData answerUser;
	@ManyToOne
	private Question question;

	public UserData getAnswerUser() 
	{
		return answerUser;
	}

	public void setAnswerUser(UserData answerUser)
	{
		this.answerUser = answerUser;
	}

	public Question getQuestion()
	{
		return question;
	}

	public void setQuestion(Question question) 
	{
		this.question = question;
	}

	public Boolean getIsApproved() 
	{
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved)
	{
		this.isApproved = isApproved;
	}

	public int getAnswerId() 
	{
		return answerId;
	}

	public void setAnswerId(int answerId)
	{
		this.answerId = answerId;
	}

	public String getAnswer() 
	{
		return answer;
	}

	public void setAnswer(String answer)
	{
		this.answer = answer;
	}

	@Override
	public String toString() 
	{
		return "Answer [answerId=" + answerId + ", answer=" + answer + ", isApproved=" + isApproved + ", answerUser="
				+ answerUser + ", question=" + question + "]";
	}

}
