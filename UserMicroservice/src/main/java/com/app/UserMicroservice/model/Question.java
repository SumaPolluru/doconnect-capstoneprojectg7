package com.app.UserMicroservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Question")
public class Question 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int questionId;
	private String questionTopic;
	private String question;
	private Boolean isApproved = false;

	@ManyToOne
	private UserData user;

	public UserData getUser() 
	{
		return user;
	}

	public void setUser(UserData user) 
	{
		this.user = user;
	}

	public Boolean getIsApproved() 
	{
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved) 
	{
		this.isApproved = isApproved;
	}

	public int getQuestionId() 
	{
		return questionId;
	}

	public void setQuestionId(int questionId)
	{
		this.questionId = questionId;
	}

	public String getQuestionTopic()
	{
		return questionTopic;
	}

	public void setQuestionTopic(String questionTopic)
	{
		this.questionTopic = questionTopic;
	}

	public String getQuestion()
	{
		return question;
	}

	public void setQuestion(String question) 
	{
		this.question = question;
	}

	@Override
	public String toString() 
	{
		return "Question [questionId:" + questionId + ", questionTopic:" + questionTopic + ", question:" + question
				+ ", isApproved:" + isApproved +"]";
	}
	
}
