package com.app.UserMicroservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

@Entity
@Table(name = "UserData")
public class UserData 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;

	public int getUserId() 
	{
		return userId;
	}

	public void setUserId(int userId) 
	{
		this.userId = userId;
	}

	@Column
	private String username;
	@Column
	@JsonIgnore // whenever record will store it will not password in normal format
	private String password;

	public String getUsername() 
	{
		return username;
	}

	public void setUsername(String username) 
	{
		this.username = username;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	

	public UserData(int userId,String username, String password) {
		super();
		this.userId=userId;
		this.username = username;
		this.password = password;
	}
	
	public UserData() 
	{
		
	}

	@Override
	public String toString() 
	{
		return "UserData [userId:" + userId + ", username:" + username + "]";
	}
	
}
