package com.app.UserMicroservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.UserMicroservice.model.UserData;

@Repository
public interface UserRepository extends JpaRepository<UserData, Integer> 
{
	public UserData findByUsername(String username);
	
	public UserData findByUserIdAndUsername(int userId,String username);

	public UserData findByUserId(int userId);

}
