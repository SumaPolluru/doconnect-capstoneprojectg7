package com.app.UserMicroservice.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.UserMicroservice.dto.AskQuestion;
import com.app.UserMicroservice.model.Question;
import com.app.UserMicroservice.model.UserData;
import com.app.UserMicroservice.repository.QuestionRepository;
import com.app.UserMicroservice.repository.UserRepository;

@Service
public class QuestionService 
{
	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	UserRepository userRepository;
	
	//PostQuestion Operation Business Logic
	public Question askQuestion(AskQuestion askQuestion) 
	{
		Question question = new Question();
		Optional<UserData> user=userRepository.findById(askQuestion.getUserId());
		question.setQuestion(askQuestion.getQuestion());
		question.setQuestionTopic(askQuestion.getQuestionTopic());
		question.setIsApproved(false);
		question.setUser(user.get());
		questionRepository.save(question);
		return question;
	}
	
	public List<Question> getApprovedQuestions()
	{
		return questionRepository.findByIsApprovedQuestions();
	}
	
	public List<Question> getQuestions()
	{
		return questionRepository.findByQuestions();
	}
	
	public List<Question> getUnApprovedQuestions() 
	{
		return questionRepository.findByIsApproved();
	}
	
   //Approve Questions
	public Question approveQuestion(int questionId) 
	{
		Question question = questionRepository.findByQuestionId(questionId);
		question.setIsApproved(true);
		question = questionRepository.save(question);
		return question;
	}
	
	public void deleteQuestion(int questionId) 
	{
		questionRepository.deleteById(questionId);
	}

	//Search Functionality
	public List<Question> searchByQuestion(String question)
	{
		return questionRepository.searchFunctionality(question);
	}
	
	public List<Question> searchByTopic(String questionTopic) 
	{
		return questionRepository.searchTopic(questionTopic);
	}

}
