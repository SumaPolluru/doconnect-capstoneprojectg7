package com.app.UserMicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.app.UserMicroservice.model.UserData;
import com.app.UserMicroservice.repository.UserRepository;

@Service
@Transactional
public class UserService 
{
	private UserRepository userRepository;

	public UserService(UserRepository userRepository) 
	{
		this.userRepository = userRepository;
	}
	
	public UserData findByUsername(String username) 
	{
		return userRepository.findByUsername(username);
	}

	//CRUD Operations on User
	public List<UserData> showAllUsers() 
	{
		List<UserData> users = new ArrayList<UserData>();
		for (UserData user : userRepository.findAll()) {
			users.add(user);
		}

		return users;
	}
	
	public UserData saveUser(UserData user) 
	{
		return userRepository.save(user);
	}

	public void deleteMyUser(int userId) 
	{
		userRepository.deleteById(userId);
	}

	public UserData editUser(int userId)
	{
		Optional<UserData> uobj = userRepository.findById(userId);
		return uobj.get();
	}

}
