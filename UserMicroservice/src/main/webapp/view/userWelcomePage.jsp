<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color: white;
	text-align: center;
	text-size: 5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/adminHome" class="navbar-brand"><font color=white
				size=4px>Welcome|User</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/questionForm"><font color=white size=4px>Ask
								Question</font></a></li>
					<li><a href="/approvedQuestionData"><font color=white
							size=4px>Approved Questions</font></a></li>
					<li><a href="/approvedAnswers"><font color=white
							size=4px>Approved Answers</font></a></li>
					<li><a href="/searchForm"><font color=white size=4px>Search
								Question</font></a></li>
					<li><a href="/searchTopicForm"><font color=white size=4px>Search
								Topic</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode=='USER_HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h1>
						<font size=8px>Welcome to DoConnect</font>
					</h1>
					<h3>User Operations</h3>
				</div>
			</div>

		</c:when>
		<c:when test="${userobj=='USER_QUESTION'}">
			<div class="container text-center">
				<h3>Ask Question</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="/askQuestion">
					<c:if test="${not empty error }">
						<div class="alert alert-danger">
							<c:out value="${error }"></c:out>
						</div>
					</c:if>
					<input type="hidden" name="questionId"
						value="${question.questionId}" />
					<div class="form-group">
						<label class="control-label col-md-3">User ID</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="userId"
								value="${question.userId}" placeholder="User ID" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Topic</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="questionTopic"
								value="${question.questionTopic}" placeholder="Topic" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Question</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="question"
								value="${question.question}" style="height: 200px" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Submit" />
					</div>
				</form>
			</div>
		</c:when>

		<c:when test="${userobj=='USER_ANSWER'}">
			<div class="container text-center">
				<h3>Post Answer</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="/postAnswer">
					<c:if test="${not empty error }">
						<div class="alert alert-danger">
							<c:out value="${error }"></c:out>
						</div>
					</c:if>
					<input type="hidden" name="answerId" value="${answer.answerId}" />
					<div class="form-group">
						<label class="control-label col-md-3">User ID</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="userId"
								value="${answer.userId}" placeholder="User ID" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Question Id</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="questionId"
								value="${answer.questionId}" placeholder="QuestionId" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Answer</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="answer"
								value="${answer.answer}" style="height: 200px" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Submit" />
					</div>
				</form>
			</div>
		</c:when>

		<c:when test="${mode=='QUESTION_DATA' }">
			<div class="container text-center" id="tasksDiv">
				<h3>Approved Questions</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Question Id</th>
								<th>Topic</th>
								<th>Question</th>
								<th>Approved Status</th>
								<th>Show Answer</th>
								<th>Post Answer</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="questiondata" items="${questiondata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${questiondata.questionId}</td>
									<td>${questiondata.questionTopic}</td>
									<td>${questiondata.question}</td>
									<td>${questiondata.isApproved}</td>
									<td><a
										href="/getAnswers?questionId=${questiondata.questionId}">Answer</a></td>
									<td><a href="/answerForm">Post Answer</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='ANSWER_DATA' }">
			<div class="container text-center" id="tasksDiv">
				<h3>ANSWER</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Answer Id</th>
								<th>User</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Approved Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="answerdata" items="${answerdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${answerdata.answerId}</td>
									<td>${answerdata.answerUser}</td>
									<td>${answerdata.question}</td>
									<td>${answerdata.answer}</td>
									<td>${answerdata.isApproved}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode=='GET_ANSWER' }">
			<div class="container text-center" id="tasksDiv">
				<h3>ANSWER</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Answer Id</th>
								<th>User</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Approved Status</th>
								<th>Post Answer</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="answerdata" items="${answerdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${answerdata.answerId}</td>
									<td>${answerdata.answerUser}</td>
									<td>${answerdata.question}</td>
									<td>${answerdata.answer}</td>
									<td>${answerdata.isApproved}</td>
									<td><a href="/answerForm">Post Answer</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='ANSWER_USER' }">
			<div class="container text-center" id="tasksDiv">
				<h3>ANSWER</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Answer Id</th>
								<th>User</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Approved Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="answerdata" items="${answerdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${answerdata.answerId}</td>
									<td>${answerdata.answerUser}</td>
									<td>${answerdata.question}</td>
									<td>${answerdata.answer}</td>
									<td>${answerdata.isApproved}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>


		<c:when test="${mode=='MODE_ASK'}">
			<div class="container text-center">
				<h3>Asked Successfully!!!!</h3>
				<hr>
			</div>
		</c:when>
		<c:when test="${mode=='POST_ANSWER'}">
			<div class="container text-center">
				<h3>Posted Successfully!!</h3>
				<hr>
			</div>
		</c:when>

		<c:when test="${mode=='SEARCH_QUESTION' }">
			<center>
				<h3>Search Question</h3>
				<hr>
				<form class="form-horizontal" method="POST"
					action="/searchByQuestion">
					Filter: <input type="text" name="question" id="question" size="100"
						th:value="${question.question}" required /> &nbsp; <input
						type="submit" value="Search" />
				</form>
			</center>
		</c:when>

		<c:when test="${mode=='SEARCH_TOPIC' }">
			<center>
				<h3>Search Topic</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="/searchByTopic">
					Filter: <input type="text" name="questionTopic" id="questionTopic"
						size="100" th:value="${question.questionTopic}" required />
					&nbsp; <input type="submit" value="Search" />
				</form>
			</center>
		</c:when>

	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>